This package is a code supplement to the paper *Anomalous Jet Tagging Via Sequence Modeling*: (arxiv link)

The package contains the same model used to produce the results of the paper, with a small dataset to provide a proof-of-concept scenario of the model.

All components of the package can be run in sequence via the run_all.sh script

```
source run_all.sh
```
